'------------------------------------------------------------------------------
' <auto-generated>
'     Este código se generó a partir de una plantilla.
'
'     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
'     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class DetalleFactura
    Public Property idDetalleFactura As Integer
    Public Property idFactura As Integer
    Public Property idComputadora As Nullable(Of Integer)
    Public Property idHora As Nullable(Of Integer)
    Public Property idArticulo As Nullable(Of Integer)

    Public Overridable Property Articulos As Articulos
    Public Overridable Property Computadoras As Computadoras
    Public Overridable Property Factura As Factura
    Public Overridable Property Horas As Horas

End Class
