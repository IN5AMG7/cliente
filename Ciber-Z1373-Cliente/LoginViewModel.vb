﻿Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports Ciber_Z1373_Cliente

Public Class LoginViewModel
    Implements ICommand, INotifyPropertyChanged
    Private _Instancia As LoginViewModel
    Private login As Login
    Shared _connection As New CiberZ1373Entities
    Private acceso As Boolean = False

    Public Property Instancia As LoginViewModel
        Get
            Return _Instancia
        End Get
        Set(value As LoginViewModel)
            _Instancia = value

        End Set
    End Property



    Public Sub New(login As Login)
        Me.Instancia = Me
        Me.login = login
    End Sub

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged

    Public Sub NotificarCambio(ByVal propiedad As String)
        RaiseEvent PropertyChanged(Me,
                New PropertyChangedEventArgs(propiedad))
    End Sub

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Select Case parameter.ToString
            Case "Login"
                Autenticar(login.Usuario.Text, login.clave.Password)
        End Select
    End Sub

    'Private Sub Form2_Unload(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.Closing 
    'e.Cancel = True
    'End Sub

    Public Function Autenticar(ByVal usuario As String, ByVal clave As String) As Boolean
        Dim _MainWindow As New MainWindow
        Dim s = (From x In _connection.Socios Where x.usuario = usuario And x.clave = clave).SingleOrDefault
        acceso = False
        Try
            With s
                .usuario = usuario
                .clave = clave
            End With
            _MainWindow.Show()
            login.Hide()
        Catch ex As Exception

        End Try


        Return Autenticar
    End Function

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function
End Class
