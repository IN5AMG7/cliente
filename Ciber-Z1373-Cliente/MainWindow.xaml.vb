﻿Imports System.Windows.DataFormats

Class MainWindow
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        'Tiempo.Text = My.Computer.Clock.LocalTime.ToLongTimeString
        Me.DataContext = New MainWindowViewModel(Me)
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub Close_Window(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        e.Cancel = True
    End Sub

End Class
