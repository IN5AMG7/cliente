﻿Imports System.ComponentModel
Imports Ciber_Z1373_Cliente

Public Class MainWindowViewModel
    Implements ICommand, INotifyPropertyChanged

    Public Sub New(mainWindow As MainWindow)
        Me.mainWindow = mainWindow
        Iniciar_Click()
        iniciado = True
        'Tiempo.Text = My.Computer.Clock.LocalTime.ToLongTimeString
        mainWindow.Inicio.Text = DateTime.Now.Hour.ToString() & ":" & DateTime.Now.Minute.ToString() & ":" & DateTime.Now.Second.ToString()
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Public Sub NotificarCambio(ByVal propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propiedad))
    End Sub

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Throw New NotImplementedException()
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function




    Dim Horas, Minutos, Segundos As Integer
    Dim iniciado As Boolean
    Dim dsp As New System.Windows.Threading.DispatcherTimer
    Private mainWindow As MainWindow

    Private Sub Iniciar_Click()
        If iniciado = False Then
            dsp.Interval = New TimeSpan(0, 0, 0, 1)
            AddHandler dsp.Tick, AddressOf intervalo
            dsp.Start()
            iniciado = True
        Else
            Horas = 0
            Minutos = 0
            Segundos = 0
        End If
    End Sub
    Public Function intervalo()
        Segundos += 1
        If Segundos = 60 Then
            Minutos += 1
            Segundos = 0
        End If
        If Minutos = 60 Then
            Minutos = 0
            Horas += 1
        End If
        mainWindow.Tiempo.Content = Format(Horas, "00") & ":" & Format(Minutos, "00") & ":" & Format(Segundos, "00")


    End Function
End Class
